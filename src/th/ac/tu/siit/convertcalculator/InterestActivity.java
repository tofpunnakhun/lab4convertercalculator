package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {
	float rate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.button1);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.button2);
		b2.setOnClickListener(this);
		
		TextView tRate = (TextView)findViewById(R.id.textView5);
		rate = Float.parseFloat(tRate.getText().toString());
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}
	
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.button1) {
			EditText p = (EditText)findViewById(R.id.editText1);
			EditText t = (EditText)findViewById(R.id.editText2);
			TextView result = (TextView)findViewById(R.id.textView3);
			
			String res = "";
			try {
				float pp = Float.parseFloat(p.getText().toString());
				float tt = Float.parseFloat(t.getText().toString());
				float re = (float)(pp*(Math.pow(1+(rate*0.01), tt)));
				res = String.format(Locale.getDefault(), "%.2f", re);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			result.setText(res);
		}
		else if (id == R.id.button2) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("rate", rate);
			startActivityForResult(i, 9999);
		}
		
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			rate = data.getFloatExtra("rate", 10.0f);
			TextView tRate = (TextView)findViewById(R.id.textView5);
			tRate.setText(String.format(Locale.getDefault(), "%.2f", rate));
		}
	}
	

}
