package th.ac.tu.siit.convertcalculator;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity
	implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button b1 = (Button)findViewById(R.id.btnLbKg);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnUSDTHB);
		b2.setOnClickListener(this);
		
		Button b3 = (Button)findViewById(R.id.interests);
		b3.setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.btnLbKg) {
			Intent i = new Intent(this, WeightActivity.class);
			i.putExtra("defaultPound", 10.0f);
			startActivity(i);
		}
		else if (id == R.id.btnUSDTHB) {
			Intent i = new Intent(this, ExchangeActivity.class);
			startActivity(i);
		}
		else if (id == R.id.interests) {
			Intent i = new Intent(this, InterestActivity.class);
			startActivity(i);
		}
		
		
	}

}
