package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WeightActivity extends Activity
	implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weight);
		
		Button b = (Button)findViewById(R.id.btnConvert);
		b.setOnClickListener(this);
		
		
		Intent i = this.getIntent();
		float defLb = i.getFloatExtra("defaultPound", 1.0f);
		//defLb = the value named 'defaultPound' in the intent,
		//			1.0f if no value named ;defaultPound' is attached
		EditText edLb = (EditText)findViewById(R.id.etLb);
		edLb.setText(String.format(Locale.getDefault(), "%.1f", defLb));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.weight, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		EditText edLb = (EditText)findViewById(R.id.etLb);
		TextView tvKg = (TextView)findViewById(R.id.tvKg);
		String res = "";
		try {
			float flb = Float.parseFloat(edLb.getText().toString());
			float fkg = flb * 0.454f;
			res = String.format(Locale.getDefault(), "%.3f", fkg);
		} catch(NumberFormatException e1) {
			res = "Invalid input";
		} catch(NullPointerException e2) {
			res = "Invalid input";
		}
		tvKg.setText(res);
	}

}
